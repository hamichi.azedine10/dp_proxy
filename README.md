The intent of the Proxy design pattern is to:
"Provide a surrogate or placeholder for another object to control access to it." [GoF]
Fournir un substitut ou un espace réservé pour un autre objet pour contrôler l'accès à celui-ci."[GoF]

The Proxy design pattern solves problems like:
How can the access to an object be controlled?
How can additional functionality be provided when accessing an object?
For example, the access to sensitive, expensive, or remote objects should be controlled.
The Proxy pattern describes how to solve such problems:
Provide a surrogate or placeholder for another object to control access to it.
Define a separate Proxy object that acts as placeholder for another object (Subject). A proxy implements the Subject interface so that it can act as placeholder anywhere a subject is expected.
Work through a Proxy object to control the access to an (already existing) object.

The Proxy design pattern provides a solution:
Define a separate Proxy object that acts as substitute for another object (Subject).
 Work through a Proxy object to control the access to a real subject.
