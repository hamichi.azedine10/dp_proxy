public class ProxyDao implements Data {
    // controler l'acces a l'objet dao

    private  String role;
    private  dao dao  = new dao();

    public ProxyDao(String role) {
        this.role = role;
    }

    @Override
    public void getdata() {
        System.out.println("verification du role ...");
        if (role=="ADMIN"){
            System.out.println("compte admin, charger toutes les données de la base de données :  ");
            dao.getdata();
        }else if(role=="USER"){
            System.out.println("compte user, charger les premier 5 lignes de la base de donées ");
            dao.setN(5);
            dao.getdata();
        }else {
            throw new RuntimeException("acces refusé : role inexsitant ");
        }
    }
}
